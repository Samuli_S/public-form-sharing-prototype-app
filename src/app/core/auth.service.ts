/* Google Firebase Authentication according to the following reference:
 * https://angularfirebase.com/lessons/google-user-auth-with-firestore-custom-data/
 */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import {AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';

interface User {
  uid: string;
  email: string;
}

@Injectable()
export class AuthService {

  user: Observable<User>;

  constructor(private router: Router, private firestore: AngularFirestore,
              private fireAuth: AngularFireAuth) {

      this.user = this.fireAuth.authState.switchMap(user => {
        if (user) {
          return this.firestore.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return Observable.of(null);
        }
      });
  }

  googleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }

  private oAuthLogin(provider) {
    return this.fireAuth.auth.signInWithPopup(provider)
    .then((credential) => {
      this.updateUserData(credential.user)
    });
  }

  private updateUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.firestore.doc(`users/${user.uid}`);
    const data: User = {
      uid: user.uid,
      email: user.email
    };

    return userRef.set(data);
  }

  signOut() {
    this.fireAuth.auth.signOut().then(() => {
      this.router.navigate(['/']);
    });
  }
}
