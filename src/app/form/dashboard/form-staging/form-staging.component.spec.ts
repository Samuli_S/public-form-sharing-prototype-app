import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormStagingComponent } from './form-staging.component';

describe('FormStagingComponent', () => {
  let component: FormStagingComponent;
  let fixture: ComponentFixture<FormStagingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormStagingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormStagingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
