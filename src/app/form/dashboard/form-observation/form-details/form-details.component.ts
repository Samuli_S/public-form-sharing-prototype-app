import { Component, OnInit } from '@angular/core';
import { FormService } from '../../../form.service';
import { QuestionForm } from '../../../models/question-form.model';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-form-details',
  templateUrl: './form-details.component.html',
  styleUrls: ['./form-details.component.css']
})
export class FormDetailsComponent implements OnInit {
  selectedForm: any;
  formId: string;
  isLoading: boolean;
  formLink: string;
  answersPerForm: number;
  // formSelectedSubscription: Subscription;

  constructor(private formService: FormService, private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.answersPerForm = 0;
    this.isLoading = true;
    this.selectedForm = null;
    this.route.params.subscribe((params: Params) => {
      this.formId = params['formId'];
      console.log('form id', this.formId);
      this.formLink = window.location.host + '/form-filling/' + this.formId;
      this.formService.getFormById(this.formId)
        .then((document) => {
          this.selectedForm = document.data();
          if (this.selectedForm.answers) {
            this.answersPerForm = this.selectedForm.answers.length / this.selectedForm.questions.length;
          } else {
            this.answersPerForm = 0;
          }
          this.isLoading = false;
      });
    });
  }

  onSeeAnswersClick(answerIndex: number) {
    if (!this.selectedForm.answers) {
      return;
    }
    this.router.navigate(['../../answers/', this.formId, +answerIndex], { relativeTo: this.route});
  }

  onPublishForm() {
    this.isLoading = true;
    this.formService.setFormPublished(this.formId).then(() => {
      this.updateView();
    });
  }

  onArchiveForm() {
    this.isLoading = true;
    this.formService.setFormArchived(this.formId).then(() => {
      this.updateView();
    });
  }

  updateView() {
    this.isLoading = true;
    this.formService.getFormById(this.formId)
      .then((document) => {
        this.selectedForm = document.data();
        this.isLoading = false;
    });
  }
}
