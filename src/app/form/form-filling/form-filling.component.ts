import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormService } from '../form.service';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { BasicDialogComponent } from '../../shared/basic-dialog/basic-dialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-form-filling',
  templateUrl: './form-filling.component.html',
  styleUrls: ['./form-filling.component.css']
})
export class FormFillingComponent implements OnInit {
  questionForm: any;
  answerFormGroup: FormGroup;
  formId: string;
  isLoading = true;

  constructor(private route: ActivatedRoute, private router: Router, private formService: FormService,
              private matDialog: MatDialog) {
  }

  ngOnInit() {
    this.questionForm = null;
    // Perform setup after component initialization in order to look more responsive.
    setTimeout(() => {
      this.route.params.subscribe((params: Params) => {
          this.formId = params['formId'];
          this.formService.getFormById(this.formId)
            .then((document) => {
              this.questionForm = document.data();
              if (this.questionForm.isArchived) {
                alert('This form has been archived (it can not be filled in). Returning to Homepage.');
                this.router.navigate(['/home']);
              }
              this.initForm();
              this.isLoading = false;
            });
        }
      );
    }, 1500);
  }

  initForm() {
    const answerControls = new FormArray([]);
    for (let i = 0; i < this.questionForm.questions.length; i++) {
      answerControls .push(
        new FormControl('', [Validators.required, Validators.maxLength(350)])
      );
    }
    this.answerFormGroup = new FormGroup({
      'answers': answerControls
    });
  }

  onSubmit() {
    const answers = [];
    const answerValues = this.answerFormGroup.value.answers;
    for (let i = 0; i < answerValues.length; i++) {
      answers.push({
        questionId: i,
        answer: answerValues[i]
      });
    }
    if (!this.questionForm.answers) {
      this.questionForm.answers = [];
    }
    for (let item of answers) {
      this.questionForm.answers.push(item);
    }
    this.formService.updateForm(this.formId, this.questionForm);
    this.openThankYouDialog();
  }

  getAnswerControls() {
    return (<FormArray>this.answerFormGroup.get('answers')).controls;
  }

  openThankYouDialog() {
    const dialogRef = this.matDialog.open(BasicDialogComponent, {
      width: '250px',
      data: {title: 'Thank you!', message: 'Great job! Thank you for answering this form.'}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.router.navigate(['/home']);
    });
  }
}
