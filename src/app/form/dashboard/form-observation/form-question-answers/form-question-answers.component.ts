import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormService } from '../../../form.service';

@Component({
  selector: 'app-form-question-answers',
  templateUrl: './form-question-answers.component.html',
  styleUrls: ['./form-question-answers.component.css']
})
export class FormQuestionAnswersComponent implements OnInit {
  answers: any;
  questionId: number;
  isLoading: boolean;
  formId: string;

  constructor(private route: ActivatedRoute, private router: Router,
              private formService: FormService) { }

  ngOnInit() {
    this.isLoading = true;
    this.answers = [];
    this.route.params.subscribe((params: Params) => {
      this.formId = params['formId'];
      this.questionId = +params['questionId'];
      this.formService.getFormById(this.formId)
        .then((document) => {
          const selectedForm = document.data();
          for (const answer of selectedForm.answers) {
            if (answer.questionId === this.questionId) {
              this.answers.push(answer);
            }
          }
          this.isLoading = false;
        });
    });
  }

  onNavigateBack() {
    this.router.navigate(['../../../details/', this.formId], { relativeTo: this.route });
  }
}
