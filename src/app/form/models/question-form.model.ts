export interface QuestionForm {
  uid: string;
  id?: string;
  isPublished?: boolean;
  isArchived?: boolean;
  name: string;
  description: string;
  // Questions and answers are separate for straightforward filling in of values.
  // Answers could be in their own collection as it has a potential for great number of writes.
  questions: string[];
  answers?: [{
    questionId: number,
    answer: string
  }];
}
