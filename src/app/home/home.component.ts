import { Component, OnInit } from '@angular/core';
import { AuthService } from '../core/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  onSingIn() {
    this.authService.googleLogin().then(() => {
      this.router.navigate(['/dashboard']);
    }).catch(error => {
      alert('Login authentication error');
    });
  }
}
