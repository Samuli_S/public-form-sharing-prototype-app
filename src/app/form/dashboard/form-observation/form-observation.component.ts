import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-form-observation',
  templateUrl: './form-observation.component.html',
  styleUrls: ['./form-observation.component.css']
})
export class FormObservationComponent implements OnInit {
  routerIsActive: boolean;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.routerIsActive = false;
  }

  onRouterOutletDeactivate() {
    this.routerIsActive = false;
  }

  routerOutletActivate() {
    this.routerIsActive = true;
  }

}
