import { Answer } from './answer.model';

export class FilledForm {
  constructor(public targetFormId: string, public answers: Answer[]) { }
}
