import { Component, OnInit } from '@angular/core';
import { FormService } from '../form.service';
import {AuthService} from '../../core/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  routerLinks: any[];

  constructor(private formService: FormService,
              public authService: AuthService) { }

  ngOnInit() {
    this.routerLinks = [
      {
        path: 'form-staging',
        label: 'Form Staging'
      },
      {
        path: 'form-observation',
        label: 'Form Observation'
      }
    ];
  }

  updateLocalData() {
    this.formService.updateLocalDataRestricted();
  }
}
