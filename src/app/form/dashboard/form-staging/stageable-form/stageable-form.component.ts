import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FormService } from '../../../form.service';

@Component({
  selector: 'app-stageable-form',
  templateUrl: './stageable-form.component.html',
  styleUrls: ['./stageable-form.component.css']
})
export class StageableFormComponent implements OnInit {
  adjustOnQuestionCount: number; // E.g. show controls at the end of form.
  questionsForm: FormGroup;

  constructor(private formService: FormService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.adjustOnQuestionCount = 3;

    const questions = new FormArray([
      new FormControl('', Validators.required)
    ]);

    this.questionsForm = new FormGroup({
      'name': new FormControl('', [Validators.required, Validators.maxLength(100)]),
      'description': new FormControl('', [Validators.required, Validators.maxLength(100)]),
      'questions': questions
    });
  }

  onSubmit() {
    console.log(this.questionsForm.value);
    this.formService.addForm(this.questionsForm.value);
    this.router.navigate(['../form-observation'], { relativeTo: this.route });
  }

  onClear() {
    (<FormArray>this.questionsForm.get('questions')).controls.splice(1);
    this.questionsForm.reset();
  }

  onAddQuestion() {
    (<FormArray>this.questionsForm.get('questions'))
      .push(new FormControl('', [Validators.required, Validators.maxLength(350)]));
  }

  onRemoveQuestion(index: number) {
    (<FormArray>this.questionsForm.get('questions')).removeAt(index);
  }

  getQuestionControls() {
    return (<FormArray>this.questionsForm.get('questions')).controls;
  }

  getQuestionCount() {
    return (<FormArray>this.questionsForm.get('questions')).length;
  }
}
