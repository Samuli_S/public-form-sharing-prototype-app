import { Injectable } from '@angular/core';
import { QuestionForm } from './models/question-form.model';

import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import {AuthService} from '../core/auth.service';

@Injectable()
export class FormService {
  questionFormsCollection: AngularFirestoreCollection<QuestionForm>;
  questionFormDocument: AngularFirestoreDocument<QuestionForm>;
  questionForms: Observable<any[]>;
  isUpdatingData: boolean;
  uid: string;

  constructor(private firestore: AngularFirestore, private authService: AuthService) {
    this.authService.user.subscribe(user => {
      this.uid = user.uid;
      this.questionFormsCollection = this.firestore.collection('questionForms',
        ref => ref.where('uid', '==', this.uid));
      this.isUpdatingData = false;
      this.updateLocalData();
    });
  }

  updateLocalDataRestricted() {
    if (this.isUpdatingData) {
      return;
    }
    this.isUpdatingData = true;
    setTimeout(() => {
      this.updateLocalData();
      this.isUpdatingData = false;
    }, 1500);
  }

  private updateLocalData() {
    this.questionForms = this.questionFormsCollection.snapshotChanges()
      .map(changes => {
        return changes.map(item => {
          const data = item.payload.doc.data() as QuestionForm;
          data.id = item.payload.doc.id;
          return data;
        });
      });
  }

  addForm(formItem: QuestionForm) {
    formItem.isPublished = true;
    formItem.isArchived = false;
    formItem.uid = this.uid;
    this.questionFormsCollection.add(formItem);
  }

  setFormArchived(id: string) {
    this.questionFormDocument = this.firestore.doc(`questionForms/${id}`);
    return this.questionFormDocument.update({
      isPublished: false,
      isArchived: true
    });
  }

  setFormPublished(id: string) {
    this.questionFormDocument = this.firestore.doc(`questionForms/${id}`);
    return this.questionFormDocument.update({
      isPublished: true,
      isArchived: false
    });
  }

  getFormById(id: string) {
    return this.firestore.doc(`questionForms/${id}`).ref.get();
  }

  updateForm(id: string, form: QuestionForm) {
    this.questionFormDocument = this.firestore.doc(`questionForms/${id}`);
    return this.questionFormDocument.update(form);
  }
}
