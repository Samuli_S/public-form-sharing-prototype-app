import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormObservationComponent } from './form-observation.component';

describe('FormObservationComponent', () => {
  let component: FormObservationComponent;
  let fixture: ComponentFixture<FormObservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormObservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormObservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
