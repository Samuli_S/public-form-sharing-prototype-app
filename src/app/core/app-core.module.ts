import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialDesignModule } from './material-design/material-design.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppRoutingModule } from './app-routing.module';
import { FormModule } from '../form/form.module';

import { HomeComponent } from '../home/home.component';
import { BasicDialogComponent } from '../shared/basic-dialog/basic-dialog.component';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';

@NgModule({
  declarations: [
    HomeComponent,
    BasicDialogComponent
  ],
  imports: [
    CommonModule,
    MaterialDesignModule,
    FlexLayoutModule,
    AppRoutingModule,
    FormModule,
  ],
  exports: [
    CommonModule,
    AppRoutingModule,
    FormModule
  ],
  entryComponents: [
    BasicDialogComponent
  ],
  providers: [AuthService, AuthGuard]
})
export class AppCoreModule { }
