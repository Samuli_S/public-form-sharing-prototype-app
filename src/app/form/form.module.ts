import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardRoutingModule } from './dashboard/dashboard-routing.module';
import { MaterialDesignModule } from '../core/material-design/material-design.module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { FormStagingComponent } from './dashboard/form-staging/form-staging.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormListComponent } from './dashboard/form-list/form-list.component';
import { StageableFormComponent } from './dashboard/form-staging/stageable-form/stageable-form.component';
import { FormObservationComponent } from './dashboard/form-observation/form-observation.component';
import { FormService } from './form.service';
import { FormDetailsComponent } from './dashboard/form-observation/form-details/form-details.component';
import { FormFillingComponent } from './form-filling/form-filling.component';
import { FormQuestionAnswersComponent } from './dashboard/form-observation/form-question-answers/form-question-answers.component';

@NgModule({
  declarations: [
    FormListComponent,
    StageableFormComponent,
    FormStagingComponent,
    DashboardComponent,
    FormObservationComponent,
    FormDetailsComponent,
    FormFillingComponent,
    FormQuestionAnswersComponent
  ],
  imports: [
    CommonModule,
    MaterialDesignModule,
    DashboardRoutingModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    DashboardRoutingModule
  ],
  providers: [
    FormService
  ]
})
export class FormModule { }
