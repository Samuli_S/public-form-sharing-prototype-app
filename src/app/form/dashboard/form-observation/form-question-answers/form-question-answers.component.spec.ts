import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormQuestionAnswersComponent } from './form-question-answers.component';

describe('FormQuestionAnswersComponent', () => {
  let component: FormQuestionAnswersComponent;
  let fixture: ComponentFixture<FormQuestionAnswersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormQuestionAnswersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormQuestionAnswersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
