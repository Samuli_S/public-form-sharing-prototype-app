import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormService } from '../../form.service';
import { QuestionForm } from '../../models/question-form.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-form-list',
  templateUrl: './form-list.component.html',
  styleUrls: ['./form-list.component.css']
})
export class FormListComponent implements OnInit, OnDestroy {
  questionForms: QuestionForm[];
  formsSubscription: Subscription;
  isLoading: boolean;

  constructor(private formService: FormService, private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.isLoading = true;
    this.formsSubscription = this.formService.questionForms.subscribe(
      (data: QuestionForm[]) => {
        this.questionForms = data;
        this.isLoading = false;
      }
    );
  }

  onFormItemClick(formId: string) {
    this.router.navigate(['details', formId], { relativeTo: this.route });
  }

  ngOnDestroy() {
    this.formsSubscription.unsubscribe();
  }
}
