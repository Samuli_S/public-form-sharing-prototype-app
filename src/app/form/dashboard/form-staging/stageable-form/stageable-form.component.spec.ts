import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StageableFormComponent } from './stageable-form.component';

describe('StageableFormComponent', () => {
  let component: StageableFormComponent;
  let fixture: ComponentFixture<StageableFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StageableFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StageableFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
