import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { FormStagingComponent } from './form-staging/form-staging.component';
import { FormObservationComponent } from './form-observation/form-observation.component';
import {FormDetailsComponent} from './form-observation/form-details/form-details.component';
import {FormQuestionAnswersComponent} from './form-observation/form-question-answers/form-question-answers.component';
import { AuthGuard } from '../../core/auth.guard';

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard], children: [
      { path: '', redirectTo: 'form-staging', pathMatch: 'full' },
      { path: 'form-staging', component: FormStagingComponent },
      { path: 'form-observation', component: FormObservationComponent, children: [
        { path: 'details/:formId', component: FormDetailsComponent },
        { path: 'answers/:formId/:questionId', component: FormQuestionAnswersComponent }
      ]}
    ]}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DashboardRoutingModule { }
